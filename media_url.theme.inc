<?php 
function theme_node_media_links($variables){
  $linksoutput = '';
  foreach($variables['links'] AS $link){
    $url = file_create_url($link['uri']);
    $rel_url = substr($url, strlen($GLOBALS['base_url']));
        
    $linksoutput .= '<div class="media-url">';
    $linksoutput .= '<p><span>Name:</span> ' . $link['filename'] . '</p>';
    $linksoutput .= '<p><span>Type:</span> ' . $link['filemime'] . '</p>';
    $linksoutput .= '<p><span>URL:</span> ' . $rel_url . '</p>';
    $linksoutput .= '<p>' . l('preview', $url, array('attributes' => array('target'=>'_blank'))) . '</p>';
    $linksoutput .= '</div>';
  }
  
  $output = '<div class="media-url-wrapper"><h3>Below is a list of the file URLs being used by this node.</h3>';
  $output .= $linksoutput;
  $output .= '</div>';
  
  return $output;
}